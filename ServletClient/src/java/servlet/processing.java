/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.Channel;

/**
 *
 * @author aguilo
 */
@WebServlet(name = "processing", urlPatterns = {"/processing"})
public class processing extends HttpServlet {
    public int resultat;
    private final static String QUEUE_NAME="Queue_test_sdis";
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            String load = request.getParameter("charge").toString();
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet processing</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<center>Servlet processing at " + request.getContextPath() + 
                    " <br /> <br /> charge : "+load+"<br /><br />");
            if(resultat==1){
                out.println("une erreur s'est produite");
            }
            else {
                out.println("Requête envoyée");
            }
            out.println("</center>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        
        String message=request.getParameter("charge").toString();
        int load = Integer.parseInt(request.getParameter("charge").toString());
        int nb = Integer.parseInt(request.getParameter("nombre").toString());
        System.out.println("charge = " + load + "x" + nb);
        
        resultat=sendMessage(message,nb);
        
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    
    public int sendMessage(String message,int nombre) throws java.io.IOException{
        int retour=0;
        try{
            ConnectionFactory factory = new ConnectionFactory();
            factory.setHost("localhost");
            Connection connection = factory.newConnection();
            Channel channel = connection.createChannel();
            message += ","+System.currentTimeMillis();
            channel.queueDeclare(QUEUE_NAME, false, false, false, null);
            if(nombre>=0){
                for(int i=0;i<nombre;i++){
                    channel.basicPublish("", QUEUE_NAME, null, message.getBytes());
                    System.out.println(" [x] Sent '" + message + "'");
                }
            }
            channel.close();
            connection.close();
        }
        catch (Exception e){
            System.out.println("exception");
            retour=1;           
        }
        
        return retour;
    }

}
