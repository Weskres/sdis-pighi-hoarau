package Server;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Guillaume
 */
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;


public class QoSManager implements Master {
	
    private ArrayList<Worker> workers;
    private long lasttimestamp;
	
    public QoSManager(){
        workers = new ArrayList<Worker>();
        workers.add(new Workers(this));
    }
	
    public void timeWaited(long time,long timestamp) {
        long space = timestamp - lasttimestamp;
	if(space > 3000){
            lasttimestamp=timestamp;
            if(time>2000){
                workers.add(new Workers(this));
                System.out.println("New worker created. Actually there is "+workers.size()+" workers");
            }
            else if(workers.size()!=1&&time<500){
                workers.get(0).stopTask();
                workers.remove(0);
                System.out.println("A worker has been removed. Actually there is "+workers.size()+" workers");
            }
        }
	try {
            Thread.sleep(500);
	} catch (InterruptedException e) {
            e.printStackTrace();
	}
    }
}