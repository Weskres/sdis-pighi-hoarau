package Server;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Guillaume
 */

import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.ConsumerCancelledException;
import com.rabbitmq.client.QueueingConsumer;
import com.rabbitmq.client.ShutdownSignalException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Workers extends Thread implements Worker {

    private final static String QUEUE_NAME = "Queue_test_sdis";
    private boolean hasToStop;
    private QoSManager qos;
    
    public Workers (QoSManager qos){
        super();
        hasToStop=false;
        this.start();
        this.qos=qos;
    }
    
    public void run(){            
        try {
            ConnectionFactory factory = new ConnectionFactory();
            factory.setHost("localhost");
            Connection connection = factory.newConnection();
            Channel channel = connection.createChannel();
            channel.queueDeclare(QUEUE_NAME, false, false, false, null);
            QueueingConsumer consumer = new QueueingConsumer(channel);
            channel.basicConsume(QUEUE_NAME, true, consumer);
            do {
                QueueingConsumer.Delivery delivery = consumer.nextDelivery();
                String message = new String(delivery.getBody());
                System.out.println(" [x] Received '" + message + "'");
                String[] listmess = message.split(",");
                qos.timeWaited(System.currentTimeMillis()-Long.parseLong(listmess[1]),System.currentTimeMillis());
                this.doWork(new Task(Integer.parseInt(listmess[0]),false));
            }while (!hasToStop);
        } catch (IOException ex) {
            Logger.getLogger(Workers.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InterruptedException ex) {
            Logger.getLogger(Workers.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ShutdownSignalException ex) {
            Logger.getLogger(Workers.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ConsumerCancelledException ex) {
            Logger.getLogger(Workers.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("Worker stopping");
    }
	
    public void doWork(Task t) {
	try {
            Thread.sleep(t.getTime());
            t.setState(true);
	} catch (InterruptedException e) {
            e.printStackTrace();
	}
    }

    public synchronized void stopTask() {
	hasToStop=true;
    }

}