package Server;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Guillaume
 */
public class Task {

    private int time;
    private boolean state;

    public Task(int time, boolean state){
	this.time=time;
	this.state=state;
    }

    public int getTime() {
	return time;
    }

    public boolean isState() {
	return state;
    }
    
    public void setState(boolean state) {
	this.state = state;
    }
	
}